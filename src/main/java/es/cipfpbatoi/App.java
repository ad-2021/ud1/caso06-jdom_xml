package es.cipfpbatoi;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

/**
 * Tratamiento de un archivo XML con JDom
 */
public class App {

	public static void main(String[] args) throws MalformedURLException {
		lectura();
		escritura();
		modificacion();
		consulta();
	}

	private static void lectura() {
		System.out.println("********** LECTURA DE UN FICHERO XML **********");
		SAXBuilder constructor = new SAXBuilder();
		Path archivoXml = Paths.get("clientes.xml");
		try {
			// Análisis sintáctio (parseo) y carga en memoria del fichero xml.
			Document doc = constructor.build(archivoXml.toFile());
			System.out.println("Tipo de documento: " + doc.getDocType());

			Element raiz = doc.getRootElement();
			System.out.println("Nombre etiqueta principal: " + raiz.getName());
			System.out.println("-----");

			List<Element> hijos = raiz.getChildren("cli");
			// Recorrido completo
			// for (int i = 0; i < hijos.size(); i++) {
			// Element nodo = (Element) hijos.get(i);
			// }
			for (Element nodoCli : hijos) {
				mostrarNodoCli(nodoCli);
			}

		} catch (JDOMException | IOException ex) {
			System.err.println(ex.getMessage());
		}
	}

	private static void escritura() {
		System.out.println("********** ESCRITURA DE UN NUEVO FICHERO XML **********");

		// Vamos a crear un xml desde cero

		// Creamos elemento raíz
		Element clientes = new Element("clientes");
		// Creamos document usando el elemento raíz creado
		Document doc = new Document(clientes);

		// Creamos primer nodo (hijo) y lo agregamos al documento
		Element cli = new Element("cli");
		cli.setAttribute(new Attribute("id", "1"));
		cli.addContent(new Element("nombre").setText("Sergio"));
		cli.addContent(new Element("apellidos").setText("Galisteo Castro"));
		cli.addContent(new Element("saldo").setText("300"));
		clientes.addContent(cli);
		// ó doc.getRootElement().addContent(cli);

		// Hacemos lo mismo con el segundo
		Element cli2 = new Element("cli");
		cli2.setAttribute(new Attribute("id", "2"));
		cli2.addContent(new Element("nombre").setText("Carlos"));
		cli2.addContent(new Element("apellidos").setText("Perales Sánchez"));
		cli2.addContent(new Element("saldo").setText("350"));
		clientes.addContent(cli2);
		// ó doc.getRootElement().addContent(cli2);

		// Escritura física en archivo
		XMLOutputter xmlsalida = new XMLOutputter();
		xmlsalida.setFormat(Format.getPrettyFormat());
//        xmlsalida.setFormat(Format.getCompactFormat());
		try {
			xmlsalida.output(doc, new FileWriter("clientes1.xml"));
			xmlsalida.output(doc, System.out);
		} catch (IOException ex) {
			System.err.println(ex.getMessage());
		}

	}

	private static void modificacion() {
		System.out.println("********** MODIFICACIÓN DE UN FICHERO XML **********");
		SAXBuilder constructor = new SAXBuilder();
		Path archivoXml = Paths.get("clientes1.xml");
		try {
			Document doc = constructor.build(archivoXml.toFile());
			Element raiz = doc.getRootElement();

			List<Element> nodos = raiz.getChildren();

			// Modificamos el nombre del cliente Carlos
			Element cli = (Element) nodos.get(1);
			cli.getChild("nombre").setText("Carlitos");

			// Modificamos el atributo de la etiqueta cli
			cli.getAttribute("id").setValue("3");

			// Añadimos un elemento más a cada nodo
			Element ciudad;
			for (Element nodoCli : nodos) {
				ciudad = new Element("ciudad");
				ciudad.setText("Alcoi");
				nodoCli.addContent(ciudad);
			}

			// Borramos elemento saldo de cada nodo
			for (Element nodoCli : nodos) {
				nodoCli.removeChild("saldo");
			}

			// Añadimos un nuevo cliente
			Element nuevoCli = new Element("cli");
			// e1.setAttribute(new Attribute("id", "2"));
			nuevoCli.setAttribute("id", "2");
			nuevoCli.addContent(new Element("nombre").setText("Pedro"));
			nuevoCli.addContent(new Element("apellidos").setText("Albero Cifuentes"));
			nuevoCli.addContent(new Element("ciudad").setText("Castalla"));
			raiz.addContent(nuevoCli);

			// Borramos un nodo cliente completo. Por ejemplo, el primer nodo.
			nodos.remove(0);

			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileWriter("clientes-modif.xml"));
			xmlOutput.output(doc, System.out);
			System.out.println("¡Fichero actualizado!");

		} catch (JDOMException | IOException ex) {
			System.err.println(ex.getMessage());
		}
	}

	private static void consulta() {
		System.out.println("********** CONSULTA DE UN FICHERO XML **********");
		SAXBuilder constructor = new SAXBuilder();
		Path archivoXml = Paths.get("clientes.xml");
		try {
			// Análisis sintáctio (parseo) y carga en memoria del fichero xml.
			Document doc = constructor.build(archivoXml.toFile());

			System.out.println("Buscando clientes de nombre Carlos ...");
			XPathExpression<Element> xpe = XPathFactory.instance().compile("//cli[nombre='Carlos']", Filters.element());
			List<Element> clienteFiltrados = xpe.evaluate(doc);			
			for (Element nodoCli : clienteFiltrados) {
				mostrarNodoCli(nodoCli);
			}

		} catch (JDOMException | IOException ex) {
			System.err.println(ex.getMessage());
		}
	}
	
	private static void mostrarNodoCli(Element nodoCli) {
		System.out.println("Identificador: " + nodoCli.getAttributeValue("id"));
		System.out.println("Nombre: " + nodoCli.getChildText("nombre"));
		// o tb. System.out.println("Nombre: "+nodo.getChild("nombre").getText());
		// o tb. System.out.println("Nombre: "+nodo.getChild("nombre").getValue());
		System.out.println("Apellidos: " + nodoCli.getChildText("apellidos"));
		System.out.println("Saldo: " + nodoCli.getChildText("saldo"));
		System.out.println("-----");
		
	}

}
